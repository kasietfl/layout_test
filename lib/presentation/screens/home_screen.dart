import 'package:flutter/material.dart';
import 'package:layout_test/utils/colors.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: backgroundColor,
      body: Center(
          child: Text(
        "Home",
        style:
            TextStyle(color: white, fontSize: 20, fontWeight: FontWeight.w600),
      )),
    );
  }
}
