import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:layout_test/presentation/widgets/buy_sell_card.dart';
import 'package:layout_test/utils/colors.dart';
import 'package:layout_test/utils/text_styles.dart';

class TradeScreen extends StatefulWidget {
  const TradeScreen({super.key});

  @override
  State<TradeScreen> createState() => _TradeScreenState();
}

class _TradeScreenState extends State<TradeScreen> {
  Map<int, bool> selectedItems = {};
  List<String> orderStrings = ["Order Book", "Traders", "Info"];

  @override
  void initState() {
    super.initState();
    selectedItems[0] = true;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: backgroundColor,
        body: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      const Icon(
                        Icons.arrow_back_ios_outlined,
                        color: lightGrey,
                        size: 18,
                      ),
                      const Icon(
                        Icons.align_horizontal_left,
                        color: white,
                      ),
                      const SizedBox(
                        width: 8,
                      ),
                      const Text(
                        "BTC/USDT",
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w600,
                            color: white),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Container(
                        padding: const EdgeInsets.all(2),
                        decoration: BoxDecoration(
                          color: lightGreen.withOpacity(0.3),
                          borderRadius: BorderRadius.circular(6),
                        ),
                        child: const Text(
                          "+2.38%",
                          style: TextStyle(
                              color: lightGreen,
                              fontSize: 14,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                    ],
                  ),
                  const Icon(
                    Icons.star_outline_rounded,
                    color: lightGrey,
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: const [
                      Text(
                        "43,810.58",
                        style: TextStyle(
                            color: lightGreen,
                            fontSize: 24,
                            fontWeight: FontWeight.w700),
                      ),
                      Text(
                        "~ \$43,810.58",
                        style: kTextStyle13,
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: const [
                      Text(
                        "24h High",
                        style: kTextStyle13,
                      ),
                      Text(
                        "24h Low",
                        style: kTextStyle13,
                      ),
                      Text(
                        "24h Volume",
                        style: kTextStyle13,
                      )
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "45,504.32",
                        style: kTextStyle13.copyWith(color: white),
                      ),
                      Text(
                        "42,128.40",
                        style: kTextStyle13.copyWith(color: white),
                      ),
                      Text(
                        "205.08M",
                        style: kTextStyle13.copyWith(color: white),
                      )
                    ],
                  )
                ],
              ),
              const SizedBox(
                height: 22,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      const Text(
                        "Time",
                        style: kTextStyle13,
                      ),
                      const SizedBox(
                        width: 22,
                      ),
                      Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 4, horizontal: 8),
                        decoration: BoxDecoration(
                            color: darkGrey,
                            borderRadius: BorderRadius.circular(6)),
                        child: Text(
                          "5m",
                          style: kTextStyle13.copyWith(color: white),
                        ),
                      ),
                      const SizedBox(
                        width: 22,
                      ),
                      const Text(
                        "15m",
                        style: kTextStyle13,
                      ),
                      const SizedBox(
                        width: 22,
                      ),
                      const Text(
                        "4H",
                        style: kTextStyle13,
                      ),
                      const SizedBox(
                        width: 22,
                      ),
                      const Text(
                        "More",
                        style: kTextStyle13,
                      ),
                    ],
                  ),
                  SvgPicture.asset(
                    'assets/icons/settings.svg',
                    height: 18,
                    width: 18,
                    colorFilter:
                        const ColorFilter.mode(lightGrey, BlendMode.srcIn),
                  ),
                ],
              ),
              const SizedBox(
                height: 18,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    padding:
                        const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                    decoration: BoxDecoration(
                        color: darkGrey,
                        borderRadius: BorderRadius.circular(6)),
                    child: const Text(
                      "MA",
                      style: kTextStyle13,
                    ),
                  ),
                  const Text(
                    "BOLL",
                    style: kTextStyle13,
                  ),
                  const Text(
                    "EMA",
                    style: kTextStyle13,
                  ),
                  const Text(
                    "Volume",
                    style: kTextStyle13,
                  ),
                  const Text(
                    "MACD",
                    style: kTextStyle13,
                  ),
                  const Text(
                    "More",
                    style: kTextStyle13,
                  ),
                ],
              ),
              const SizedBox(
                height: 18,
              ),
              SizedBox(
                height: 30,
                child: ListView.separated(
                  itemCount: 3,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      onTap: () {
                        if (selectedItems[index] == null) {
                          selectedItems[index] = false;
                        }
                        for (int i = 0; i < 3; i++) {
                          selectedItems[i] = false;
                        }
                        selectedItems[index] = true;

                        setState(() {});
                      },
                      child: Container(
                        height: 24,
                        padding: const EdgeInsets.symmetric(horizontal: 12),
                        decoration: BoxDecoration(
                          color: selectedItems[index] ?? false
                              ? darkGrey
                              : Colors.transparent,
                          borderRadius:
                              const BorderRadius.all(Radius.circular(6)),
                        ),
                        child: Center(
                          child: Text(
                            orderStrings[index],
                            style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                                color: selectedItems[index] ?? false
                                    ? white
                                    : lightGrey),
                          ),
                        ),
                      ),
                    );
                  },
                  separatorBuilder: (BuildContext context, int index) {
                    return const SizedBox(width: 12);
                  },
                ),
              ),
              const SizedBox(
                height: 16,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: const [
                  BuySellCard(
                    isSell: false,
                    quantities: ["0.1600343", "0.2500543", "0.324341"],
                    prices: ["43,805", "43,804", "43,798"],
                  ),
                  BuySellCard(
                      isSell: true,
                      quantities: ["0.1600343", "0.2500543", "0.324341"],
                      prices: ["43,805", "43,804", "43,798"])
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
