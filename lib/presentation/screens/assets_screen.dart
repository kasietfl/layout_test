import 'package:flutter/material.dart';
import 'package:layout_test/utils/colors.dart';

class AssetsScreen extends StatelessWidget {
  const AssetsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: backgroundColor,
      body: Center(
          child: Text(
        "Assets",
        style:
            TextStyle(color: white, fontSize: 20, fontWeight: FontWeight.w600),
      )),
    );
  }
}
