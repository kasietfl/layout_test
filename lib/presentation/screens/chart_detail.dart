import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:layout_test/presentation/widgets/detail_info.dart';
import 'package:layout_test/presentation/widgets/large_button.dart';
import 'package:layout_test/presentation/widgets/statictics_box.dart';
import 'package:layout_test/utils/colors.dart';
import 'package:syncfusion_flutter_sliders/sliders.dart';

class ChartDetail extends StatefulWidget {
  const ChartDetail({super.key});

  @override
  State<ChartDetail> createState() => _ChartDetailState();
}

class _ChartDetailState extends State<ChartDetail> {
  bool isChecked = true;
  double sliderValue = 0;
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: const BouncingScrollPhysics(),
      padding: const EdgeInsets.all(20),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 14.0),
                child: Row(
                  children: [
                    const Icon(
                      Icons.align_horizontal_left,
                      color: lightGrey,
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    const Text(
                      "BTC/USDT",
                      style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w600,
                          color: white),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Container(
                      padding: const EdgeInsets.all(2),
                      decoration: BoxDecoration(
                        color: lightGreen.withOpacity(0.3),
                        borderRadius: BorderRadius.circular(6),
                      ),
                      child: const Text(
                        "+2.38%",
                        style: TextStyle(
                            color: lightGreen,
                            fontSize: 14,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                  ],
                ),
              ),
              Row(
                children: [
                  SvgPicture.asset(
                    'assets/icons/settings2.svg',
                    height: 15,
                    width: 10,
                    colorFilter:
                        const ColorFilter.mode(lightGrey, BlendMode.srcIn),
                  ),
                  const SizedBox(
                    width: 8,
                  ),
                  const Icon(
                    Icons.more_horiz,
                    color: lightGrey,
                  ),
                ],
              )
            ],
          ),
          Row(
            children: [
              SizedBox(
                width: MediaQuery.of(context).size.width / 2 + 20,
                child: Column(
                  children: [
                    const LargeButton(
                      title: "43,805.35",
                      color: darkGrey,
                      showIcons: true,
                      textColor: white,
                    ),
                    const SizedBox(
                      height: 14,
                    ),
                    const LargeButton(
                      title: "Amount (BTC)",
                      color: darkGrey,
                      showIcons: true,
                      textColor: lightGrey,
                    ),
                    const SizedBox(
                      height: 14,
                    ),
                    SfSlider(
                      min: 0.0,
                      max: 100.0,
                      value: sliderValue,
                      interval: 20,
                      showDividers: true,
                      thumbIcon: const Icon(
                        Icons.menu,
                        size: 12,
                        color: white,
                      ),
                      minorTicksPerInterval: 1,
                      onChanged: (dynamic value) {
                        setState(() {
                          sliderValue = value;
                        });
                      },
                    ),
                    const SizedBox(
                      height: 14,
                    ),
                    const LargeButton(
                      title: "Total (USDT)",
                      color: darkGrey,
                      showIcons: false,
                      textColor: lightGrey,
                    ),
                    const SizedBox(
                      height: 14,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: const [
                        Text(
                          "Avbl",
                          style: TextStyle(
                              fontSize: 13,
                              fontWeight: FontWeight.w600,
                              color: lightGrey),
                        ),
                        Text(
                          "1 000.00 USDT",
                          style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                              color: white),
                        ),
                        CircleAvatar(
                            backgroundColor: lightGrey,
                            radius: 7,
                            child: Icon(
                              Icons.add,
                              size: 12,
                              color: white,
                            )),
                      ],
                    ),
                    const SizedBox(
                      height: 14,
                    ),
                    const LargeButton(
                      title: "Buy BTC",
                      color: lightGreen,
                      showIcons: false,
                      textColor: white,
                    ),
                  ],
                ),
              ),
              const SizedBox(width: 20),
              SizedBox(
                width: MediaQuery.of(context).size.width / 2 - 80,
                child: Column(
                  children: [
                    const StaticticsBox(
                        fillPercent: 6,
                        text: '43,805',
                        price: '20,5K',
                        colorText: coral),
                    const StaticticsBox(
                        fillPercent: 5,
                        text: '43,804',
                        price: '20,1K',
                        colorText: coral),
                    const StaticticsBox(
                        fillPercent: 4,
                        text: '43,798',
                        price: '19,5K',
                        colorText: coral),
                    const StaticticsBox(
                        fillPercent: 3,
                        text: '43,796',
                        price: '19,2K',
                        colorText: coral),
                    const StaticticsBox(
                        fillPercent: 2,
                        text: '43,794',
                        price: '18,5K',
                        colorText: coral),
                    const StaticticsBox(
                      fillPercent: 1,
                      text: '43,792',
                      price: '18,3K',
                      colorText: coral,
                    ),
                    const SizedBox(height: 10),
                    const Text(
                      '43,810.58',
                      style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                          color: lightGreen),
                    ),
                    const Text(
                      '\$43,810.58',
                      style: TextStyle(
                          fontSize: 13,
                          fontWeight: FontWeight.w600,
                          color: lightGrey),
                    ),
                    const SizedBox(height: 10),
                    const StaticticsBox(
                        fillPercent: 1, text: '43,811', price: '15,2K'),
                    const StaticticsBox(
                        fillPercent: 2, text: '43,812', price: '16,1K'),
                    const StaticticsBox(
                        fillPercent: 3, text: '43,714', price: '17,5K'),
                    const StaticticsBox(
                        fillPercent: 4, text: '43,716', price: '19,2K'),
                    const StaticticsBox(
                        fillPercent: 5, text: '43,717', price: '18,5K'),
                    const StaticticsBox(
                        fillPercent: 6, text: '43,720', price: '21,3K'),
                    const SizedBox(height: 28),
                    Row(
                      children: [
                        Flexible(
                          child: Container(
                            height: 30,
                            padding: const EdgeInsets.symmetric(
                                vertical: 4, horizontal: 5),
                            decoration: BoxDecoration(
                                color: lightGrey.withOpacity(0.5),
                                borderRadius:
                                    const BorderRadius.all(Radius.circular(7))),
                            child: Row(
                              children: const [
                                Text(
                                  '0.01',
                                  style: TextStyle(
                                      fontSize: 13,
                                      fontWeight: FontWeight.w600,
                                      color: white),
                                ),
                                Icon(Icons.arrow_drop_down, color: white)
                              ],
                            ),
                          ),
                        ),
                        const SizedBox(width: 7),
                        Container(
                            padding: const EdgeInsets.symmetric(
                                vertical: 4, horizontal: 5),
                            width: 30,
                            height: 30,
                            decoration: BoxDecoration(
                                color: lightGrey.withOpacity(0.5),
                                borderRadius:
                                    const BorderRadius.all(Radius.circular(7))),
                            child: Row(
                              children: [
                                Column(
                                  children: const [
                                    Element(),
                                    Spacer(),
                                    Element(
                                      color: lightGreen,
                                    ),
                                  ],
                                ),
                                const Spacer(),
                                Column(
                                  children: const [
                                    Element(
                                      color: lightGrey,
                                      width: 11,
                                      height: 5,
                                    ),
                                    Spacer(),
                                    Element(
                                      color: lightGrey,
                                      width: 11,
                                      height: 5,
                                    ),
                                    Spacer(),
                                    Element(
                                      color: lightGrey,
                                      width: 11,
                                      height: 5,
                                    ),
                                  ],
                                )
                              ],
                            ))
                      ],
                    ),
                    const SizedBox(height: 15),
                  ],
                ),
              )
            ],
          ),
          const SizedBox(
            height: 18,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Container(
                    width: 14,
                    height: 14,
                    decoration: BoxDecoration(
                        border: Border.all(
                            color:
                                isChecked == true ? blue : Colors.transparent,
                            width: 2),
                        borderRadius: (BorderRadius.circular(5.0))),
                    child: Checkbox(
                      value: isChecked,
                      onChanged: (value) => setState(() {
                        isChecked = !isChecked;
                      }),
                      activeColor: blue,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0)),
                      side: const BorderSide(
                          width: 2.0, color: Color(0xffDEDEDE)),
                      checkColor: white,
                    ),
                  ),
                  const SizedBox(
                    width: 12,
                  ),
                  const Text(
                    "Show all",
                    style: TextStyle(color: lightGrey),
                  ),
                ],
              ),
              SizedBox(
                height: 24,
                child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(6.0),
                        ),
                        backgroundColor: darkGrey),
                    onPressed: () {},
                    child: const Text(
                      "Cancel all",
                      style:
                          TextStyle(fontSize: 14, fontWeight: FontWeight.w600),
                    )),
              ),
            ],
          ),
          const SizedBox(
            height: 18,
          ),
          const Divider(
            color: darkGrey,
          ),
          const SizedBox(
            height: 18,
          ),
          const DetailInfo(
            title: "TBCC",
            date: "2022-03-06 17:50:12",
            type: "Sell",
            color: coral,
            amount: "4407,32",
            price: "0,0673",
          ),
          const SizedBox(
            height: 18,
          ),
          const Divider(
            color: darkGrey,
          ),
          const SizedBox(
            height: 18,
          ),
          const DetailInfo(
            title: "BTC",
            date: "2022-03-05 16:45:24",
            type: "Buy",
            color: lightGreen,
            amount: "0,32500543",
            price: "23,900",
          )
        ],
      ),
    );
  }
}

class Element extends StatelessWidget {
  final Color? color;
  final double? height;
  final double? width;
  const Element(
      {super.key, this.color = coral, this.height = 7, this.width = 7});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      decoration: BoxDecoration(
          color: color,
          borderRadius: const BorderRadius.all(Radius.circular(2))),
    );
  }
}
