import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:layout_test/presentation/screens/assets_screen.dart';
import 'package:layout_test/presentation/screens/charts_screen.dart';
import 'package:layout_test/presentation/screens/home_screen.dart';
import 'package:layout_test/presentation/screens/markets_screen.dart';
import 'package:layout_test/utils/colors.dart';

class HomeRoot extends StatefulWidget {
  const HomeRoot({super.key});

  @override
  State<HomeRoot> createState() => _HomeRootState();
}

class _HomeRootState extends State<HomeRoot> {
  int _selectedIndex = 0;

  final _navigatorKeys = [
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
  ];
  final PageController _pageController = PageController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        controller: _pageController,
        physics: const NeverScrollableScrollPhysics(),
        children: [
          Navigator(
            key: _navigatorKeys[0],
            onGenerateRoute: (route) => CupertinoPageRoute(
              builder: (context) => const HomeScreen(),
            ),
          ),
          Navigator(
            key: _navigatorKeys[1],
            onGenerateRoute: (route) => CupertinoPageRoute(
              builder: (context) => const MarketsScreen(),
            ),
          ),
          Navigator(
            key: _navigatorKeys[2],
            onGenerateRoute: (route) => CupertinoPageRoute(
              builder: (context) => const ChartsScreen(),
            ),
          ),
          Navigator(
            key: _navigatorKeys[3],
            onGenerateRoute: (route) => CupertinoPageRoute(
              builder: (context) => const AssetsScreen(),
            ),
          )
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: backgroundColor,
        currentIndex: _selectedIndex,
        selectedItemColor: white,
        unselectedItemColor: lightGrey,
        showUnselectedLabels: true,
        type: BottomNavigationBarType.fixed,
        selectedFontSize: 12,
        selectedLabelStyle: const TextStyle(fontWeight: FontWeight.w600),
        unselectedLabelStyle: const TextStyle(fontWeight: FontWeight.w600),
        onTap: bottomOnTap,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            activeIcon: SvgPicture.asset(
              'assets/icons/home.svg',
              colorFilter: const ColorFilter.mode(blue, BlendMode.srcIn),
              height: 24,
              width: 24,
            ),
            icon: SvgPicture.asset(
              'assets/icons/home.svg',
              height: 24,
              width: 24,
              colorFilter: const ColorFilter.mode(lightGrey, BlendMode.srcIn),
            ),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            activeIcon: SvgPicture.asset(
              'assets/icons/markets.svg',
              colorFilter: const ColorFilter.mode(blue, BlendMode.srcIn),
              height: 24,
              width: 24,
            ),
            icon: SvgPicture.asset(
              'assets/icons/markets.svg',
              height: 24,
              width: 24,
              colorFilter: const ColorFilter.mode(lightGrey, BlendMode.srcIn),
            ),
            label: 'Markets',
          ),
          BottomNavigationBarItem(
            activeIcon: SvgPicture.asset(
              'assets/icons/trade.svg',
              colorFilter: const ColorFilter.mode(blue, BlendMode.srcIn),
              height: 24,
              width: 24,
            ),
            icon: SvgPicture.asset(
              'assets/icons/trade.svg',
              height: 24,
              width: 24,
              colorFilter: const ColorFilter.mode(lightGrey, BlendMode.srcIn),
            ),
            label: 'Trade',
          ),
          BottomNavigationBarItem(
            activeIcon: SvgPicture.asset(
              'assets/icons/assets.svg',
              colorFilter: const ColorFilter.mode(blue, BlendMode.srcIn),
              height: 24,
              width: 24,
            ),
            icon: SvgPicture.asset(
              'assets/icons/assets.svg',
              height: 24,
              width: 24,
              colorFilter: const ColorFilter.mode(lightGrey, BlendMode.srcIn),
            ),
            label: 'Assets',
          ),
        ],
      ),
    );
  }

  void bottomOnTap(index) {
    setState(() {
      if (index != 4) {
        if (index == _selectedIndex) {
          _navigatorKeys[index]
              .currentState!
              .popUntil((route) => route.isFirst);
        } else {
          _selectedIndex = index;
          _pageController.jumpToPage(index);
        }
      }
    });
  }
}
