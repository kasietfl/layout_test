import 'package:flutter/material.dart';
import 'package:layout_test/presentation/screens/chart_detail.dart';
import 'package:layout_test/presentation/screens/trade_screen.dart';

import 'package:layout_test/utils/colors.dart';

class ChartsScreen extends StatefulWidget {
  const ChartsScreen({super.key});

  @override
  State<ChartsScreen> createState() => _ChartsScreenState();
}

class _ChartsScreenState extends State<ChartsScreen>
    with SingleTickerProviderStateMixin {
  late TabController tabController;
  @override
  void initState() {
    tabController = TabController(length: 2, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: backgroundColor,
        body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(20),
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.transparent,
                    border: Border.all(color: lightGrey),
                    borderRadius: const BorderRadius.all(Radius.circular(7))),
                height: 30,
                width: double.infinity,
                child: Center(
                  child: TabBar(
                      overlayColor:
                          MaterialStateProperty.all(Colors.transparent),
                      controller: tabController,
                      indicator: const BoxDecoration(
                          color: lightGrey,
                          borderRadius: BorderRadius.all(Radius.circular(7))),
                      labelColor: white,
                      unselectedLabelColor: lightGrey,
                      unselectedLabelStyle:
                          const TextStyle(color: lightGrey, fontSize: 14),
                      labelStyle: const TextStyle(color: white, fontSize: 14),
                      tabs: const [
                        Tab(
                          iconMargin: EdgeInsets.zero,
                          text: "Charts",
                        ),
                        Tab(
                          iconMargin: EdgeInsets.zero,
                          text: "Trade",
                        ),
                      ]),
                ),
              ),
            ),
            Expanded(
                child: TabBarView(
                    controller: tabController,
                    children: const [ChartDetail(), TradeScreen()]))
          ],
        ),
      ),
    );
  }
}
