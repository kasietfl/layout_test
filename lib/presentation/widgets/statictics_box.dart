import 'package:flutter/material.dart';
import 'package:layout_test/utils/colors.dart';

class StaticticsBox extends StatelessWidget {
  final int fillPercent;
  final String text;
  final String price;
  final Color? colorText;
  const StaticticsBox(
      {super.key,
      required this.fillPercent,
      required this.price,
      this.colorText = lightGreen,
      required this.text});

  @override
  Widget build(BuildContext context) {
    final double fillStop = (100 - fillPercent * 15) / 100;
    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(tileMode: TileMode.mirror, colors: [
        Colors.transparent,
        Colors.transparent,
        colorText!.withOpacity(0.3),
        colorText!.withOpacity(0.3)
      ], stops: [
        0,
        fillStop,
        fillStop,
        1
      ])),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            text,
            style: TextStyle(
                fontSize: 12, fontWeight: FontWeight.w600, color: colorText),
          ),
          Text(
            price,
            style: const TextStyle(
                fontSize: 12, fontWeight: FontWeight.w600, color: white),
          )
        ],
      ),
    );
  }
}
