import 'package:flutter/material.dart';
import 'package:layout_test/utils/colors.dart';

class BuySellCard extends StatelessWidget {
  final List<String> quantities;
  final List<String> prices;
  final bool isSell;
  const BuySellCard(
      {super.key,
      required this.isSell,
      required this.quantities,
      required this.prices});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: MediaQuery.of(context).size.width / 2 - 24,
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  isSell ? "Sell Price" : "Quantity",
                  style: const TextStyle(
                      fontSize: 11, fontWeight: FontWeight.w700, color: grey),
                ),
                Text(
                  isSell ? "Quantity" : "Buy Price",
                  style: const TextStyle(
                      fontSize: 11, fontWeight: FontWeight.w700, color: grey),
                )
              ],
            ),
            const SizedBox(
              height: 6,
            ),
            TextRow(
              quantity: quantities[0],
              price: prices[0],
              isSell: isSell,
            ),
            const SizedBox(
              height: 4,
            ),
            TextRow(
              quantity: quantities[1],
              price: prices[1],
              isSell: isSell,
            ),
            const SizedBox(
              height: 4,
            ),
            TextRow(
              quantity: quantities[2],
              price: prices[2],
              isSell: isSell,
            ),
          ],
        ));
  }
}

class TextRow extends StatelessWidget {
  final String quantity;
  final String price;
  final bool isSell;
  const TextRow(
      {super.key,
      required this.quantity,
      required this.price,
      required this.isSell});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          isSell ? price : quantity,
          style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w600,
              color: isSell ? coral : white),
        ),
        Text(isSell ? quantity : price,
            style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w600,
                color: isSell ? white : lightGreen))
      ],
    );
  }
}
