import 'package:flutter/material.dart';
import 'package:layout_test/utils/colors.dart';

class LargeButton extends StatelessWidget {
  final String title;
  final bool showIcons;
  final Color color;
  final Color textColor;
  const LargeButton(
      {super.key,
      required this.title,
      required this.showIcons,
      required this.color,
      required this.textColor});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 10),
      height: 42,
      decoration:
          BoxDecoration(color: color, borderRadius: BorderRadius.circular(6)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          showIcons
              ? const Icon(
                  Icons.remove,
                  color: lightGrey,
                  size: 18,
                )
              : const SizedBox(),
          Text(
            title,
            style: TextStyle(
                fontSize: 16, fontWeight: FontWeight.w600, color: textColor),
          ),
          showIcons
              ? const Icon(
                  Icons.add,
                  color: lightGrey,
                  size: 18,
                )
              : const SizedBox()
        ],
      ),
    );
  }
}
