// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';

import 'package:layout_test/presentation/widgets/small_button.dart';
import 'package:layout_test/utils/colors.dart';
import 'package:layout_test/utils/text_styles.dart';

class DetailInfo extends StatelessWidget {
  final String title;
  final String date;
  final String type;
  final Color color;
  final String amount;
  final String price;
  const DetailInfo({
    Key? key,
    required this.date,
    required this.type,
    required this.color,
    required this.amount,
    required this.price,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              "$title / USDT",
              style: const TextStyle(
                  color: white, fontSize: 14, fontWeight: FontWeight.w700),
            ),
            Text(
              date,
              style: const TextStyle(
                  color: lightGrey, fontWeight: FontWeight.w600),
            )
          ],
        ),
        Text(
          "Limit / $type",
          style: TextStyle(
              color: color, fontSize: 13, fontWeight: FontWeight.w700),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: const [
                Text(
                  "Amount",
                  style: kTextStyle13,
                ),
                Text(
                  "Price",
                  style: kTextStyle13,
                ),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                RichText(
                  text: TextSpan(
                    text: '0,00',
                    style: kTextStyle13,
                    children: <TextSpan>[
                      TextSpan(
                        text: '/$amount',
                        style: kTextStyle13,
                      ),
                    ],
                  ),
                ),
                Text(
                  price,
                  style: kTextStyle13.copyWith(color: white),
                ),
              ],
            ),
            Column(
              children: const [SmallButton()],
            )
          ],
        ),
      ],
    );
  }
}
