import 'package:flutter/material.dart';

const lightGreen = Color(0xff20C997);
const coral = Color(0xffF72F72);
const backgroundColor = Color(0xff212630);
const lightGrey = Color(0xff7D8696);
const grey = Color(0xffADB5BD);
const white = Color(0xffFFFFFF);
const blue = Color(0xff1F62F1);
const darkGrey = Color(0xff333A47);
